import { cs } from '@dixid/tiny-redux';

import { is } from '@constants/store';
import { TestStore, TestState, IsNavigationOpen, OnTestAction, TestAction } from '@interfaces/store';
import { tr } from '@reducers/store';

let store: TestStore;

const onTestAction: OnTestAction = () => {
  const dispatch = store[1];
  const action: TestAction = 'c0';
  dispatch(action);
};

const showNavigation: IsNavigationOpen = () => {
  const getStore = store[0];
  const state = getStore();
  return state[1][0];
};

const createTestStore = (): TestStore => {
  const tmpStore = cs<TestState, TestAction>(tr, is);
  store = [...tmpStore, onTestAction, showNavigation];
  return store;
};

export const useTestStore = (): TestStore => {
  return store ? store : createTestStore();
};

export { TestStore, TestState, OnTestAction, IsNavigationOpen, TestAction };
