import { Store, State } from '@dixid/tiny-redux';

export interface StoreStateSettings {
  [0]: boolean;
}

export interface TestState extends State {
  [0]: string;
  [1]: StoreStateSettings;
}

export type TestAction = 'c0';

export type IsNavigationOpen = () => boolean;

export type OnTestAction = () => void;

export type TestStore = [...Store<TestState, TestAction>, OnTestAction, IsNavigationOpen];
