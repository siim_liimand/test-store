import { Reducer } from '@dixid/tiny-redux';

import { TestAction, TestState } from '@interfaces/store';

const testReducer: Reducer<TestState, TestAction> = (state, action) => {
  switch (action) {
    case 'c0': {
      return {
        ...state,
        [1]: {
          [0]: !state[1][0],
        },
      };
    }
    default:
      return state;
  }
};

export { testReducer as tr };
