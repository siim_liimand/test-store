import { TestState } from '@interfaces/store';

const initialState: TestState = {
  [0]: '',
  [1]: {
    [0]: true,
  },
};

export { initialState as is };
