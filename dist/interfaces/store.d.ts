import { Store, State } from '@dixid/tiny-redux';
export interface StoreStateSettings {
    [0]: boolean;
}
export interface TestState extends State {
    [0]: string;
    [1]: StoreStateSettings;
}
export declare type TestAction = 'c0';
export declare type IsNavigationOpen = () => boolean;
export declare type OnTestAction = () => void;
export declare type TestStore = [...Store<TestState, TestAction>, OnTestAction, IsNavigationOpen];
//# sourceMappingURL=store.d.ts.map