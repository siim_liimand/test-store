"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useTestStore = void 0;
const tiny_redux_1 = require("@dixid/tiny-redux");
const store_1 = require("./constants/store");
const store_2 = require("./reducers/store");
let store;
const onTestAction = () => {
    const dispatch = store[1];
    const action = 'c0';
    dispatch(action);
};
const showNavigation = () => {
    const getStore = store[0];
    const state = getStore();
    return state[1][0];
};
const createTestStore = () => {
    const tmpStore = tiny_redux_1.cs(store_2.tr, store_1.is);
    store = [...tmpStore, onTestAction, showNavigation];
    return store;
};
const useTestStore = () => {
    return store ? store : createTestStore();
};
exports.useTestStore = useTestStore;
//# sourceMappingURL=index.js.map