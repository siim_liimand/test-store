import { Reducer } from '@dixid/tiny-redux';
import { TestAction, TestState } from '../interfaces/store';
declare const testReducer: Reducer<TestState, TestAction>;
export { testReducer as tr };
//# sourceMappingURL=store.d.ts.map