"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tr = void 0;
const testReducer = (state, action) => {
    switch (action) {
        case 'c0': {
            return {
                ...state,
                [1]: {
                    [0]: !state[1][0],
                },
            };
        }
        default:
            return state;
    }
};
exports.tr = testReducer;
//# sourceMappingURL=store.js.map