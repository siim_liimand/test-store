import { TestStore, TestState, IsNavigationOpen, OnTestAction, TestAction } from './interfaces/store';
export declare const useTestStore: () => [import("@dixid/tiny-redux").GetState<TestState>, import("@dixid/tiny-redux").Dispatch<"c0">, import("@dixid/tiny-redux").Subscribe, OnTestAction, IsNavigationOpen];
export { TestStore, TestState, OnTestAction, IsNavigationOpen, TestAction };
//# sourceMappingURL=index.d.ts.map